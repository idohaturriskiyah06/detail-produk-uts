@extends('layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>CRUD Detail Produk | Laravel 8</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('produks.create') }}"> Tambah Produk</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Nama Produk</th>
            <th>Rincian</th>
            <th width="280px">Pilihan</th>
        </tr>
        @foreach ($produks as $produk)
        <tr>
            <td>{{ $produk->nama_produk }}</td>
            <td>{{ $produk->detail_produk }}</td>
            <td>
                <form action="{{ route('produks.destroy',$produk->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('produks.show',$produk->id) }}">Lihat</a>
    
                    <a class="btn btn-primary" href="{{ route('produks.edit',$produk->id) }}">Sunting</a>
   
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $produks->links() !!}
      
@endsection