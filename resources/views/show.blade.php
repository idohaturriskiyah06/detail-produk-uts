@extends('layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>PRODUK</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('produks.index') }}"> Kembali</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Produk:</strong>
                {{ $produk->nama_produk }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Rincian Produk:</strong>
                {{ $produk->detail_produk }}
            </div>
        </div>
    </div>
@endsection